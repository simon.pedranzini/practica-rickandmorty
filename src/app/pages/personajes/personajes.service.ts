import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { personajesI } from 'src/app/models/personajes.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonajesService {

  constructor(private http: HttpClient) { }

  getAllCharacters(page: number): Observable<personajesI>{
    return this.http.get<personajesI>(`https://rickandmortyapi.com/api/character/?page=${page}`);
  }
}
